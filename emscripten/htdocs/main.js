let songs = [
		'PMD/Shiori%20Ueno/Kotohigaoka%20Story/m001.m',
		'PMD/- unknown/Advanced Variable Geo 2/avg2ayb.m',
		'PMD/- unknown/Xenon - Fantasy Body (OPN2)/xen01-t.m',
		'PMD/- unknown/Brandish 1/br1-01.m',
		'MDX/- unknown/Green Beret/grba.mdx',
		'MDX/- unknown/Final Fantasy 5/ff5a101.mdx',
		'MDX/- unknown/Final Fantasy 1/ff1-01.mdx',
		'MDX/- unknown/Marble Madness/mbl07.mdx',
	];

class MDXDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle()		{ return "webMDX";}
	getDisplaySubtitle()	{ return "日本のゲーム音楽";}
	getDisplayLine1() { return this.getSongInfo().title; }
	getDisplayLine2() { return this.getSongInfo().artist; }
	getDisplayLine3() { return ""; }
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new MDXBackendAdapter();
		this._backend.setProcessorBufSize(2048);

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {


			let makeOptionsFromUrl = function(someSong) {
					// drag&dropped temp files start with "/tmp/"
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, {}];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new MDXDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x505050,0xffffff,0x404040,0xffffff], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}