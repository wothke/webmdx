/*
* This file adapts "_mdxmini/pmdmini" and exports the relevant API to JavaScript, e.g. for
* use in my generic JavaScript player.
*
* Copyright (C) 2018 Juergen Wothke
*
*
* Credits:
*   
*  milk.K, K.MAEKAWA, Missy.M - authors of the original X68000 MXDRV
*  Daisuke Nagano - author of the Unix mdxplay
*  KAJIHARA Mashahiro - original author of the PMD sound driver for PC-9801
*  AGAWA Koji - Maintainer of PMDXMMS, on which pmdmini was based
*  M88 / cisc - author of OPNA FM sound generator 
*  BouKiCHi - author of the _mdxmini&pmdmini library
*  Misty De Meo - bugfixes and improvements to _mdxmini&pmdmini
*  
*  The project is based on: https://github.com/mistydemeo/_mdxmini & https://github.com/mistydemeo/pmdmini
*
*
* License:
*
*  The code builds on FM Sound Generator with OPN/OPM interface Copyright (C) by cisc.
*  As the author of FM Sound Generator states (see readme.txt in "fmgen" sub-folder), this is free software but 
*  for any commercial application the prior consent of the author is explicitly required. So it probably 
*  cannot be GPL.. and the GPL claims made in the pdmmini project are probably just invalid. 
*  
*  Be that as it may.. I'll extend whatever license is actually valid for the underlying code to the 
*  code that I added here to turn "_mdxmini/pmdmini" into a JavaScript lib.
*/

#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>     

#include <iostream>
#include <fstream>

extern "C" {
#include "mdxmini.h"
#include "../pmdmini/src/pmdmini.h"

// use global flag to limit API changes in existing libs: directly set by
// respective patched lib code
int g_file_not_ready = 0;		
}

#include "MetaInfoHelper.h"

using emsutil::MetaInfoHelper;


#define CHANNELS			2				
#define BYTES_PER_SAMPLE	2
#define RAW_INFO_MAX		1024

namespace mdx {

	class Adapter {
	public:
		Adapter() : _samplesAvailable(0), _maxPlayLength(-1), _currentPlayLength(0), _mdx_mode(0), 
					_initialized(false),  _sampleBuffer(0), _sampleBufSize(0), _sampleRate(0)
		{
		}
		
		int getSampleRate()
		{
			return _sampleRate;
		}
		
		void teardown () 
		{
			MetaInfoHelper::getInstance()->clear();
				
			if (_initialized) {
				if (_mdx_mode) {	
					mdx_close(&_mdxmini);
				} else {
					pmd_stop();
				}
				_initialized = false;
			}
		}
		
		void allocBuffers(uint32_t audioBufSize) 
		{
			if (_sampleBufSize != audioBufSize ) 
			{
				if(_sampleBuffer) free(_sampleBuffer);
				_sampleBuffer = 0;
			}
			if (!_sampleBuffer) 
			{
				_sampleBuffer = (int16_t*)malloc(sizeof(int16_t) * audioBufSize * CHANNELS);
			}
			_sampleBufSize = audioBufSize;
		}
		
		int loadFile(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)  
		{
			// fixme: scopesEnabled support not implemented yet
			
			_sampleRate = sampleRate;
			allocBuffers(audioBufSize);
								
			initEmu(filename);
			
			int r;
			if ((r = loadSong(filename))) 
			{
				setSongInfo();
			} 
			return !r;
		}

		void setSongInfo() 
		{	
			MetaInfoHelper *info = MetaInfoHelper::getInstance();
			
			if (_mdx_mode) 
			{	
				_maxPlayLength = mdx_get_length(&_mdxmini);	// len in secs

				mdx_set_max_loop(&_mdxmini, 0);
				mdx_get_title(&_mdxmini, _tmpTxtBuffer);
			} 
			else 
			{
				_maxPlayLength= pmd_length_sec();
				
				pmd_get_compo(_tmpTxtBuffer);
				info->setText(1, _tmpTxtBuffer, "");

				pmd_get_title( _tmpTxtBuffer );
			}
			info->setText(0, _tmpTxtBuffer, "");
		}
		
		int genSamples() 
		{
			if ((_maxPlayLength > 0) && (_currentPlayLength >= _maxPlayLength)) return 1;	// stop song

			if (_mdx_mode) 
			{	
				mdx_calc_sample(&_mdxmini, _sampleBuffer, _sampleBufSize);
			} 
			else 
			{
				pmd_renderer ( _sampleBuffer, _sampleBufSize );		
			}
			_samplesAvailable = _sampleBufSize;
			_currentPlayLength += ((double)_samplesAvailable) / _sampleRate;
			return 0;
		}
		
		char* getSampleBuffer() 
		{
			return (char*)_sampleBuffer;
		}

		int getSampleBufferLength() 
		{
			return _samplesAvailable;
		}
		
		int getCurrentPosition() 
		{
			return _currentPlayLength * 1000;
		}
		
		int getMaxPosition() 
		{
			return _maxPlayLength * 1000;
		}
		
	private:
		void initEmu(char *filename) 
		{
			_mdx_mode = endsWith(std::string(filename), std::string(".mdx")) || endsWith(std::string(filename), std::string(".MDX"));
			
			if (_mdx_mode) 
			{	
				memset(&_mdxmini, 0, sizeof(t_mdxmini));
				mdx_set_rate(_sampleRate);	
			} 
			else 
			{
				pmd_init();
				pmd_setrate( _sampleRate );
			}
			_initialized = true;
		}

		int loadSong0(const char *filename) 
		{
			if (_mdx_mode) 
			{
				int r = mdx_open(&_mdxmini, (char*)filename, (char*) NULL);
				return g_file_not_ready == -1 ? -1 : r;
			} 
			else 
			{
				return pmd_play(filename, (char*) NULL );
			}
		}
		
		int loadSong(const char *filename) 
		{
			g_file_not_ready = 0;
			
			_maxPlayLength = -1;
			_currentPlayLength = 0;
			if (loadSong0(filename)) 
			{
				printf("File open error: %s\n", filename);
				return 0;
			}
			return 1;
		}
		
		int endsWith(std::string const & value, std::string const & ending) 
		{
			if (ending.size() > value.size()) { return 0; }
			
			return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
		}
	private:
		int _sampleRate;
		int _sampleBufSize;
		
		int16_t *_sampleBuffer;
		int _samplesAvailable;
		int _maxPlayLength;
		double _currentPlayLength;

			// emulators
		int _mdx_mode;
		t_mdxmini _mdxmini;
		bool _initialized;
		
		char _tmpTxtBuffer[RAW_INFO_MAX];
	};

};

mdx::Adapter _adapter;


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func 
	

EMBIND(int, emu_load_file(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) { 
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return 0;	/*there are no subsongs*/ }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int frames))			{ }
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }
